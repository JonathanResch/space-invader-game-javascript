function assert(test_name,condition) {
  'use strict';
  if (!condition) {
    var msg = 'Assertion failed';
    if (test_name)
      msg = msg + ': ' + test_name;
    throw new Error(msg);
  } else {
    return !!test_name ? (test_name + ":- test passed") : "test passed";
  }
}